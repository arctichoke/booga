#ifndef __BOOGA_H
#define __BOOGA_H

#define BOOGA_MAJOR 0   /* default major number */
#define BOOGA_DEVS  4   /* booga0 through booga3 */

#define BOOGA       "booga! booga! "
#define BOOGA_LEN   14//sizeof(BOOGA)
#define GOOGOO      "googoo! gaagaa! "
#define GOOGOO_LEN  16//sizeof(GOOGOO)
#define NEKA        "neka! maka! "
#define NEKA_LEN    12//sizeof(NEKA)
#define WOOGA       "wooga! wooga! "
#define WOOGA_LEN   14//sizeof(WOOGA)

#define NUM_PHRASES     4

struct booga_device {
    struct semaphore sem;   /* mutex */
    size_t reads;  /* No of times device read called. */
    size_t writes; /* No of times device write called. */
    size_t opens;  /* No of times device opened. */
    size_t bytes_read; /* Total bytes read from the device. */
    size_t bytes_written; /* Total bytes written to the device. */
    unsigned int number;    /* minor number */
    char *buf;
};

#endif
