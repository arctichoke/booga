#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/semaphore.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/string.h>
#include <linux/random.h>
#include <linux/uaccess.h>
#include <linux/sched.h>
#include <linux/signal.h>
#include "booga.h"

static int booga_major = BOOGA_MAJOR;
static int booga_devs  = BOOGA_DEVS;

static char *phrases[NUM_PHRASES] = {BOOGA, GOOGOO, NEKA, WOOGA};
static int  phrase_lens[NUM_PHRASES] = {BOOGA_LEN, GOOGOO_LEN, NEKA_LEN, WOOGA_LEN};
static size_t phrase_hits[NUM_PHRASES] = {0};
static struct semaphore hits_mutex;     /* Need to protect hits with a mutex */

static struct booga_device* booga_devices[BOOGA_DEVS];
static struct proc_dir_entry* proc_file_entry;

static int booga_proc_show(struct seq_file *m, void *v) {
    int i;
    size_t reads[BOOGA_DEVS];
    size_t opens[BOOGA_DEVS];
    size_t read_size = 0, write_size = 0;

    for (i = 0; i < booga_devs; i++) {
        reads[i] = booga_devices[i]->reads;
        opens[i] = booga_devices[i]->opens;
        write_size += booga_devices[i]->bytes_written;
        read_size += booga_devices[i]->bytes_read;
    }

    seq_printf(m, "bytes read    =\t%-zu\n", read_size);
    seq_printf(m, "bytes written =\t%-zu\n", write_size);
    seq_printf(m, "number of opens\n");

    for (i = 0; i < booga_devs; i++) {
        seq_printf(m, "\t/dev/booga%d = %-zu\n", i, opens[i]);
    }

    seq_printf(m, "string output:\n");
    for (i = 0; i < NUM_PHRASES; i++) {
        seq_printf(m, "\t%s= %zu\n", phrases[i], phrase_hits[i]);
    }
    return 0;
}

static int booga_proc_open(struct inode *inode, struct file* file) {
    return single_open(file, booga_proc_show, NULL);
}

static int booga_release (struct inode *inode, struct file *filp) {
    return 0;
}

static int booga_open(struct inode *inode, struct file *filp) {
    struct booga_device *dev;
    int num = MINOR(inode->i_rdev);

    if (num >= booga_devs) return -ENODEV;

    filp->private_data = booga_devices[num]; /* pointer to the device. */
    dev = booga_devices[num];
    dev->number = num;

    if (down_interruptible(&(dev->sem)))
        return -ERESTARTSYS;

    dev->opens++;
    up(&(dev->sem));

    return 0;
}

static ssize_t booga_read(struct file *filp, char __user *buf,
        size_t count, loff_t *f_pos) {
    struct booga_device *dev;
    char rand_val;
    int choice, i;
    dev = (struct booga_device*) filp->private_data;

    get_random_bytes(&rand_val, 1);
    choice = (rand_val & 0x7F) % booga_devs;
    dev->buf = (char *)kmalloc((count) * sizeof(char), GFP_KERNEL);

    /* if buffer too big just return ENOMEM */
    if (!dev->buf)
        return -ENOMEM;

    if (down_interruptible(&hits_mutex))
        return -ERESTARTSYS;
    phrase_hits[choice]++;
    up(&hits_mutex);

    /* copy the phrases to the buffer */
    for (i = 0; i < count; i++)
    {
        dev->buf[i] = phrases[choice][i % phrase_lens[choice]];
    }
    count -= copy_to_user(buf, dev->buf, count); /* returns number of bytes left to copy */
    kfree(dev->buf);

    if (down_interruptible(&(dev->sem)))
        return -ERESTARTSYS;
    dev->reads++;
    dev->bytes_read += count;
    up(&(dev->sem));

    return count;
}

static ssize_t booga_write(struct file *filp, const char __user *buf,
        size_t count, loff_t *f_pos) {
    struct booga_device *dev = (struct booga_device*)filp->private_data;

    if (dev->number == 3) { /* Terminate writes to /dev/booga3 */
        send_sig(SIGTERM, current, 0);
        printk(KERN_INFO"KILL this\n");
        return 0;
    }
    if (down_interruptible(&(dev->sem)))
        return -ERESTARTSYS;

    dev->bytes_written += count;
    up(&(dev->sem));
    printk(KERN_WARNING"Bytes written %zu\n", count);
    return count;
}

static const struct file_operations booga_fops = {
    .owner = THIS_MODULE,
    .read = booga_read,
    .write = booga_write,
    .open = booga_open,
    .release = booga_release,
};

static const struct file_operations booga_proc_fops = {
    .owner = THIS_MODULE,
    .open = booga_proc_open,
    .read = seq_read,
    .llseek = seq_lseek,
    .release  = single_release,
};

static void booga_dev_init(struct booga_device *dev) {
    sema_init(&(dev->sem), 1);
}

static int __init booga_init(void) {
    int result, i;
    result = register_chrdev(booga_major, "booga", &booga_fops);

    if (result < 0) { /* If failed to assign major number exit */
        printk(KERN_WARNING "booga: can't get major number");
        return result;
    }

    if (booga_major == 0) booga_major = result; /* Accept default */

    /* Allocate a device structure for each device and initialize it to zero */
    for (i = 0; i < booga_devs; i++) {
        booga_devices[i] = (struct booga_device*) kmalloc(sizeof(struct booga_device), GFP_KERNEL);
        memset(booga_devices[i], 0, sizeof(struct booga_device));
        if (!booga_devices[i]) {
            result = -ENOMEM;
            i--;
            goto failed_malloc;
        }
    }

    proc_file_entry = proc_create("driver/booga", 0, NULL, &booga_proc_fops);
    if (!proc_file_entry) {
        result = -ENOMEM;
        goto failed_proc;
    }

    for (i = 0; i < booga_devs; i++)
        booga_dev_init(booga_devices[i]);

    sema_init(&hits_mutex, 1);

    return 0;

failed_proc:
failed_malloc:
    for (;i >= 0; i--)  /* Free all devices allocated */
        kfree(booga_devices[i]);
    unregister_chrdev(booga_major, "booga");
    return result;
}

static void __exit booga_exit(void) {
    int i;
    remove_proc_entry("driver/booga", NULL);
    for (i = 0; i < booga_devs; i++) {
        kfree(booga_devices[i]);
    }
    unregister_chrdev(booga_major, "booga");
}

module_init(booga_init);
module_exit(booga_exit);
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Suraj Deuja");
